const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const FaturaStates = [
    'Taslak',
    'KesmeyeHazir',
    'Gonderildi',
    'BasariliKesildi',
    'Hata'
]

var SchemaModel = new Schema({
    key             : String,
    vfirma          : String,
    duzenlemeTarihi : Date,
    aciklama        : String,
    vkesenFirma     : String,
    belgeOid        : String,
    faturaNo        : String,
    status          : String,

    //status          : { type: String, enum: FaturaStates },
    //kesenPersonel   : { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    //firma           : { type: mongoose.Schema.Types.ObjectId, ref: 'Firma' },
    //kesenFirma      : { type: mongoose.Schema.Types.ObjectId, ref: 'Tenant' },
    //aciklama        : String,
    //duzenlemeTarihi : Date,
    //vadeTarihi      : Date,
    //vadeGunu        : Number,
    //pdf             : String,
    //hataAciklama    : String,
    //ettn            : String,
    //faturaNo        : String,
    //belgeNo         : String,
    //ubl             : String,
    //genelToplam     : Number,
    //toplamKDV       : Number,
    //toplam          : Number,
    //tahsilatToplam  : Number,
},
    {
        toJSON: { virtuals: true },
        toObject: { virtuals: true }
    })


SchemaModel.virtual('firma', {
    ref: 'Firma',
    localField: 'vfirma',
    foreignField: 'key',
    justOne: true
})

SchemaModel.virtual('kesenFirma', {
    ref: 'Tenant',
    localField: 'vkesenFirma',
    foreignField: 'key',
    justOne: true
})


module.exports = mongoose.models.Fatura || mongoose.model('Fatura', SchemaModel)