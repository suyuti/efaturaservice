const mongoose = require('mongoose')
const Schema = mongoose.Schema

const SchemaModel = new Schema({
    fatura  : { type: mongoose.Schema.Types.ObjectId, ref: 'Fatura'},
    date    : Date,
    aciklama: String
})

module.exports = mongoose.models.FaturaLog || mongoose.model('FaturaLog', SchemaModel)