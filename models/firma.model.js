const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var SchemaModel = new Schema({
    key                 : { type: String},
    // ...
    efaturaTCKNKullan   : Boolean,  // Faturada VergiNo yerine TCKN kullaniliyorsa
    efaturaTCKN         : String,   // Faturada VergiNo yerine TCKN kullaniliyorsa
    efaturaTCKNAdi      : String,   // Faturada VergiNo yerine TCKN kullaniliyorsa ADI kullanilir
    efaturaTCKNSoyadi   : String    // Faturada VergiNo yerine TCKN kullaniliyorsa SOYADI kullanilir
})

module.exports = mongoose.models.Firma || mongoose.model('Firma', SchemaModel)