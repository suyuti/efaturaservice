
const AMQP_HOST = process.env.AMQP_URL || "amqp://localhost";
//const QUEUE_NAME = 'amqp-connection-manager-sample2'
//const EXCHANGE_NAME = 'amqp-connection-manager-sample2-ex';

const IN_EXCHANGE_NAME = 'efatura-inEx'
const IN_QUEUE_NAME = 'efatura-in'
const OUT_EXCHANGE_NAME = 'efatura-outEx'
const OUT_QUEUE_NAME = 'efatura-out'


// Handle an incomming message.
var inChannel
var outChannel

exports.start = () => {
    const amqp = require('amqp-connection-manager');
    // Create a connetion manager
    const connection = amqp.connect([AMQP_HOST]);
    connection.on('connect', () => console.log('Connected!'));
    connection.on('disconnect', err => console.log('Disconnected.', err.stack));


    inChannel = connection.createChannel({
        setup: channel => {
            // `channel` here is a regular amqplib `ConfirmChannel`.
            return Promise.all([
                channel.assertQueue(IN_QUEUE_NAME, { exclusive: true, autoDelete: true }),
                channel.assertExchange(IN_EXCHANGE_NAME, 'topic'),
                channel.prefetch(1),
                channel.bindQueue(IN_QUEUE_NAME, IN_EXCHANGE_NAME, '#'),
                channel.consume(IN_QUEUE_NAME, onMessage)
            ])
        }
    })
    inChannel.waitForConnect()

    //----------------------------------------------------------------------------

    outChannel = connection.createChannel({
        json: true,
        setup: channel => channel.assertExchange(OUT_EXCHANGE_NAME, 'topic')
    })

    const sendMessage = (payload) => {
        outChannel.publish(OUT_EXCHANGE_NAME, "test", payload, { contentType: 'application/json', persistent: true })
            .then(() => {
                console.log('message sent')
            })
            .catch(err => {
                console.log('Message rejected: ', err.stack)
                outChannel.close()
                connection.close()
            })
    }








    // Set up a channel listening for messages in the queue.
    //channelWrapper = connection.createChannel({
    //    setup: channel => {
    //        // `channel` here is a regular amqplib `ConfirmChannel`.
    //        return Promise.all([
    //            channel.assertQueue(QUEUE_NAME, { exclusive: true, autoDelete: true }),
    //            channel.assertExchange(EXCHANGE_NAME, 'topic'),
    //            channel.prefetch(1),
    //            channel.bindQueue(QUEUE_NAME, EXCHANGE_NAME, '#'),
    //            channel.consume(QUEUE_NAME, onMessage)
    //        ])
    //    }
    //});
    //
    //channelWrapper.waitForConnect()
    //    .then(function () {
    //        console.log("Listening for messages");
    //    });

    const onMessage = data => {
        var message = JSON.parse(data.content.toString());
        console.log("subscriber: got message", message);
        inChannel.ack(data);
    }
}

exports.send = async (payload) => {
    outChannel.publish(IN_EXCHANGE_NAME, "test", payload, { contentType: 'application/json', persistent: true })
        .then(() => {
            console.log('message sent')
        })
        .catch(err => {
            console.log('Message rejected: ', err.stack)
            outChannel.close()
            connection.close()
        })
}
exports.stop = () => { }
exports.isListening = () => { }