/*
const AMQP_HOST = process.env.AMQP_HOST;
const FATURA_QUEUE = process.env.FATURA_QUEUE;
const url = process.env.AMQP_URL || "amqp://localhost";
const amqplib = require("amqplib/callback_api");
const EFaturaService = require('./efatura.service')

var amqp = require('amqp-connection-manager');
var connection = amqp.connect([url]);
var channelWrapper = connection.createChannel({
    json: true,
    setup: function(channel) {
        // `channel` here is a regular amqplib `ConfirmChannel`.
        // Note that `this` here is the channelWrapper instance.
        return channel.assertQueue('rxQueueName', {durable: true});
    }
});


//----------------------------------------------------------------------




let gConnection = null
let listening = false

exports.start = () => {
    return new Promise((resolve, reject) => {
        if (gConnection) {
            reject('Calisan dinleyici var')
        }
        amqplib.connect(url, (err, connection) => {
            if (err) {
                reject(err.stack)
            }
            else {
                connection.createChannel((err, channel) => {
                    if (err) {
                        reject(err.stack)
                    }
                    else {
                        channel.assertQueue(FATURA_QUEUE, { durable: true }, err => {
                            if (err) {
                                console.log(err)
                            }
                            channel.prefetch(1)
                            channel.consume(FATURA_QUEUE, data => {
                                if (data == null) {
                                    reject()
                                }

                                channel.checkQueue(FATURA_QUEUE, (err, ok) => {
                                    // { queue: 'experto-faturagonderx', messageCount: 4, consumerCount: 1 }
                                })

                                let faturaId = JSON.parse(data.content.toString())
                                EFaturaService.eFaturaKes(faturaId, step => {
                                    global.io.emit('step', step)
                                }).then(resp => {
                                    channel.ack(data)
                                })
                            })
                            gConnection = connection
                            listening = true
                            console.log('ok baglandi')
                            resolve({ connect: true, queue: FATURA_QUEUE })
                        })
                    }
                })
                //console.log(`connected to ${AMQP_HOST}`)
            }
        })
    })
}

exports.stop = () => {
    return new Promise((resolve, reject) => {
        if (!gConnection) {
            reject('Acik dinleyici yok')
        }
        gConnection.close(err => {
            if (err) {
                reject(err)
            }
            gConnection = null
            listening = false
            resolve({ connect: false })
        })
    })
}

exports.isListening = () => {
    return listening
}

exports.send = (faturaId) => {
    return new Promise((resolve, reject) => {
        if (gConnection) {
            gConnection.createChannel((err, channel) => {
                if (err) {
                    reject('channel acilamadi')
                }
                channel.assertQueue(FATURA_QUEUE, { durable: true }, err => {
                    if (err) {

                    }
                    let sender = content => {
                        let sent = channel.sendToQueue(FATURA_QUEUE,
                            Buffer.from(JSON.stringify(content)), { persistent: true, contentType: 'application/json' })
                        if (sent) {
                            console.log('kuyruga eklendi')
                            return channel.close(() => {
                                resolve()
                            })
                        }
                        else {
                            channel.once('drain', () => next())
                        }
                    }
                    sender(faturaId)
                })
            })
        }
        else {
            reject('Connection yok')
        }
    })
}
*/

/*
amqplib.connect(AMQP_HOST, (err, connection) => {
    if (err) {
        console.error(err.stack);
        return
    }
    console.log(`connected to ${AMQP_HOST}`)
    connection.createChannel((err, channel) => {
        if (err) {
            console.log(err)
            return
        }
        channel.assertQueue(FATURA_QUEUE, { durable: true }, (err) => {
            if (err) {
                console.log(err)
            }
            console.log(`Queue ok ${FATURA_QUEUE}`)
            channel.prefetch(1);
            channel.consume(FATURA_QUEUE, (data) => {
                if (data == null) {
                    return
                }
                console.log('FATURA')
                let fatura = JSON.parse(data.content.toString())

                if (fatura.command == 'gidenBelgeleriSorgula') {
                    FaturaController.tenantIcinGidenBelgeleriSorgula(fatura.tenant)
                    return channel.ack(data)
                }
                else {
                    FaturaController.sendFatura(fatura).then(resp => {
                        if (resp == 'next') {
                            //return channel.nackAll(false)
                            return channel.nack(data,false, true)
                        }
                        console.log('OK')
                        return channel.ack(data)
                    })
                }

            })
        })
    })

})
*/