const md5 = require('md5')
const soap = require("soap");
var _client = null

exports.createClient = (url, user, password) => {
    return new Promise((resolve, reject) => {
        const wsSecurity = new soap.WSSecurity(user, password, {})
        soap.createClient(url, (err, client) => {
            if (err) {
                reject(err)
            }
            else {
                client.setSecurity(wsSecurity);
                _client = client

                resolve({
                    faturaNoUret,
                    efaturaKullanicisiMi,
                    belgeGonder,
                    gidenBelgeSorgula
                })
            }
        })
    })
}

//----------------------------------------------------------------------------------------------------------

const faturaNoUret = (tenant, gecmisTarihli) => {
    return new Promise((resolve, reject) => {
        _client.faturaNoUret({ vknTckn: tenant.vkn, faturaKodu: gecmisTarihli ? tenant.gecmisTarihFaturaKodu : tenant.faturaKodu }, (err, response) => {
            if (err) {
                reject(err.message)
            }
            else {
                resolve(response.return)
            }
        })
    })
}

//----------------------------------------------------------------------------------------------------------

const efaturaKullanicisiMi = (vkn) => {
    return new Promise((resolve, reject) => {
        _client.efaturaKullaniciBilgisi({ vergiTcKimlikNo: vkn }, (err, response) => {
            if (err) {
                reject(err.message)
            }
            else {
                resolve(response.return)
            }
        })
    })
}

//----------------------------------------------------------------------------------------------------------

const belgeGonder = (fatura) => {
    return new Promise((resolve, reject) => {
        let ublBase64 = Buffer.from(fatura.ubl).toString('base64')
        let hash = md5(fatura.ubl)
    
        _client.belgeGonder({
            vergiTcKimlikNo : fatura.kesenFirma.vkn,
            belgeTuru       : 'FATURA_UBL',
            belgeNo         : fatura.key, //fatura.uuid,
            veri            : ublBase64,
            belgeHash       : hash,
            mimeType        : 'application/xml'
        }, (err, response) => {
            if (err) {
                reject(err.message)
            }
            else {
                resolve(response.belgeOid)
            }
        })
    })
}

//----------------------------------------------------------------------------------------------------------
const gidenBelgeSorgula = (gonderenVkn, belgeOid) => {
    return new Promise((resolve, reject) => {
        _client.gidenBelgeDurumSorgula({
            vergiTcKimlikNo : gonderenVkn,
            belgeOid        : belgeOid,
        }, (err, res) => {
            if (err) {
                reject(err)
            }
            else {
                resolve(res.return)
            }
        })
    })
}

//----------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------
