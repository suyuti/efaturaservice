var winston = require('../config/winston.config');
const FirmaModel = require('../models/firma.model')
const FaturaModel = require('../models/fatura.model')
const UrunModel = require('../models/urun.model')
const FaturaLogModel = require('../models/faturalog.model')
const FaturaKalemModel = require('../models/faturakalem.model')
const TenantModel = require('../models/tenant.model')
const QnbService = require('./qnb.service')
const makeUbl = require('../helper/ubl.helper')
const numbro = require('numbro')
const moment = require('moment')
const fs = require('fs')

exports.eFaturalariKes = async (fatura) => {
}

const onEFaturaStep = async (fatura, message, onStep) => {
    await new FaturaLogModel({fatura: fatura._id, date: new Date(), aciklama: message}).save()
    onStep({_id: fatura._id, message: message})
}

exports.eFaturaKes = async (id, onStep) => {
    let fatura = null
    try {
        winston.log(`error`, `EFatura kes`)
        // 1. Faturayi bul
        fatura = await FaturaModel
            .findById(id)
            .populate('firma')
            .populate('kesenFirma')
            .lean(true)
            .exec()
        //winston.log(`error`, JSON.stringify(fatura, null, 2))
        if (!fatura) {
            return false
        }
        //onEFaturaStep(fatura, 'Fatura bulundu', onStep)
        if (fatura.status != 'KesmeyeHazir') {
            // TODO
        }

        if (fatura.firma.eFaturaMi == false || fatura.firma.vergiNo == '' || fatura.firma.vergiDairesi == '') {
            // TODO
        }

        // Fatura kalemlerini al
        let kalemler = await FaturaKalemModel.find({vfatura: fatura.key}).populate('urun').lean(true).exec()
        //onEFaturaStep(fatura, 'Fatura kalemleri bulundu', onStep)

        //let tenant = await TenantModel.findOne({key: fatura.vkesenFirma}).lean(true).exec()

        // 2. QNBye baglan
        let client = await QnbService.createClient(fatura.kesenFirma.efaturaUrl, fatura.kesenFirma.efaturaUser, fatura.kesenFirma.efaturaPwd)
        if (!client) {
            // TODO 
        }
        //onEFaturaStep(fatura, 'QNBye baglandi', onStep)

        let aliciEFaturaListesi = await client.efaturaKullanicisiMi(fatura.firma.vergiNo)

        if (aliciEFaturaListesi.length == 0) {
            throw `${fatura.firma.unvan} eFatura Kullanicisi Degil`
            // TODO
        }
        let eFaturaKullanicisiMi = aliciEFaturaListesi.some(l => l.vergiTcKimlikNo == fatura.firma.vergiNo)
        if (!eFaturaKullanicisiMi) {
            throw `${fatura.firma.unvan} eFatura Kullanicisi Degil`
        }
    
        //onEFaturaStep(fatura, 'Alici firma EFatura firmasi', onStep)
        //onEFaturaStep(fatura, 'Tamam', onStep)

        // Yeni fatura no al
        // Duzenleme tarihi gecmis zamanli ise gecmis tarih serisinden alinacak
        console.log(fatura.duzenlemeTarihi)
        let faturaDuzenlemeTarihi   = moment(fatura.duzenlemeTarihi).startOf('day')
        let bugun                   = moment().startOf('day')
        let gecmisTarihliFatura = false
        console.log(faturaDuzenlemeTarihi)
        console.log(bugun)

        if (faturaDuzenlemeTarihi.isBefore(bugun)) {
            console.log('Gecmis Tarihli Fatura kesilecek')
            gecmisTarihliFatura = true
        }

        let faturaNo = fatura.kesenFirma.sonFaturaNo
        let sonFaturaNo = gecmisTarihliFatura ? fatura.kesenFirma.sonGecmisTarihFaturaNo : fatura.kesenFirma.sonFaturaNo
        let tryCount = 5
        do {
            faturaNo = await client.faturaNoUret(fatura.kesenFirma, gecmisTarihliFatura)
            console.log(faturaNo)
            if (!faturaNo) {
                // TODO
            }
            if (--tryCount <= 0) {
                throw `Son Fatura No surekli ayni geliyor Gelen: [${faturaNo}] - Bizdeki Son Fatura: [${sonFaturaNo}] tenant: [${fatura.kesenFirma.key}]`
            }
        } while(faturaNo == sonFaturaNo)
        //onEFaturaStep(fatura, `Fatura No alindi ${faturaNo}`, onStep)
        console.log(`Fatura No: ${faturaNo}`)

        fatura.faturaNo = faturaNo;
        fatura.ubl      = makeUbl(fatura, kalemler)

        //
        //fs.writeFileSync('./logs/ubl.xml', fatura.ubl)
        //winston.log('error', fatura.ubl)
        console.log(fatura.ubl)

        let belgeOid = await client.belgeGonder(fatura)
        if (!belgeOid) {
        }
        console.log(belgeOid)

        let gonderimDurumu = 1
        do {
            await new Promise(resolve => setTimeout(resolve, 5000));

            let gidenBelgeDurum = await client.gidenBelgeSorgula(fatura.kesenFirma.vkn, belgeOid)
            gonderimDurumu = gidenBelgeDurum.durum
            
            if (gidenBelgeDurum.durum == 2) {
                //onStep(`Gonderilen belgede hata var: ${gidenBelgeDurum.aciklama}`)
                //onStep({_id: id, message: `Gonderilen belgede hata var: ${gidenBelgeDurum.aciklama}`})
                // Hata
                throw gidenBelgeDurum.aciklama
            }
            else if (gidenBelgeDurum.durum == 1) {
                console.log('Belge hala islenmemis. Bekleniyor...')
                await new Promise(resolve => setTimeout(resolve, 5000));
            }
        } while(gonderimDurumu == 1)

        console.log('Belge islenmis')

        //onStep(`Belge gonderim durumu ${gonderimDurumu}`)
        //onStep({_id: id, message: `Belge gonderim durumu ${gonderimDurumu}`})
        
        //onEFaturaStep(fatura, `Belge gonderim durumu ${gonderimDurumu}`, onStep)

        
        fatura.belgeOid = belgeOid
        fatura.status = 'Gonderildi'
        
        let f = await FaturaModel.findByIdAndUpdate(fatura._id, fatura, {new: true, lean: true}).exec()
        console.log('Fatura kaydedildi')

        let tenantUpdateData = {}
        if (gecmisTarihliFatura) {
            tenantUpdateData.sonGecmisTarihFaturaNo = fatura.faturaNo
            tenantUpdateData.sonGecmisTarihFaturaTarihi = fatura.duzenlemeTarihi
        }
        else {
            tenantUpdateData.sonFaturaNo = fatura.faturaNo
            tenantUpdateData.sonFaturaTarihi = fatura.duzenlemeTarihi
        }
        await TenantModel.findOneAndUpdate({key: fatura.vkesenFirma}, tenantUpdateData).exec()
        console.log('Tenant guncellendi')
        //onStep('Fatura guncellendi')
        //onStep({_id: id, message: 'Fatura guncellendi'})
        //onEFaturaStep(fatura, 'Fatura guncelendi', onStep)

        return f
        
        //await fatura.save()
    }
    catch (e) {
        console.log(e)
        //onStep(`Hata: ${e}`)
        //onEFaturaStep(fatura, `Hata: ${e}`, onStep)

        //onStep({_id: id, message: `Hata: ${e}`})
    }
}

exports.topluEFaturaKes = async (fatura) => {
}

exports.onizleme = async (fatura) => {
}
