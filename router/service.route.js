const express = require('express')
const router = express.Router()

const ServiceController = require('../controllers/service.controller')

router.get('/',                ServiceController.service)
router.post('/toggle',                ServiceController.serviceToggle)
router.post('/gonderx10',                ServiceController.gonderx10)

module.exports = router
