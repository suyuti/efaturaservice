const express = require('express')
const router = express.Router()

const SettingsController = require('../controllers/settings.controller')

router.get('/',                SettingsController.settings)
router.post('/',                SettingsController.kaydet)

module.exports = router
