const express = require('express')
const router = express.Router()

const EFaturaController = require('../controllers/efatura.controller')
/*
    Fatura kes
    Statusu KES olan tum faturalari keser 
    POST

    Fatura kes :id
    POST /:id

    Fatura kes [:id]
    POST {data: []}

    Toplu Fatura kes :id
    POST /:id

    OnIzleme :id
    Belge Indir :id
*/

//router.post('/',                EFaturaController.eFaturalariKes)
router.post('/:id',             EFaturaController.eFaturaKes)
//router.post('/toplufatura/:id', EFaturaController.topluEFaturaKes)
//router.get('/onizleme/:id',     EFaturaController.onizleme)

module.exports = router
