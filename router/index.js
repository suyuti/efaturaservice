const routes = require('express').Router();
const efatura = require('./efatura.route');
const fatura = require('./fatura.route');
const home = require('./home.route');
const settings = require('./settings.route');
const service = require('./service.route');
const faturalog = require('./faturalog.route');

routes.use('/api/v1/efatura', efatura);
routes.use('/fatura', fatura);
routes.use('/settings', settings);
routes.use('/service', service);
routes.use('/faturalog', faturalog);
routes.use('/', home);

module.exports = routes;
