const express = require('express')
const router = express.Router()

const FaturaLogController = require('../controllers/faturalog.controller')

router.get('/',                FaturaLogController.getAll)

module.exports = router
