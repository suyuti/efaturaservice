const express = require('express')
const router = express.Router()

const FaturaController = require('../controllers/fatura.controller')
/*
    Fatura kes
    Statusu KES olan tum faturalari keser 
    POST

    Fatura kes :id
    POST /:id

    Fatura kes [:id]
    POST {data: []}

    Toplu Fatura kes :id
    POST /:id

    OnIzleme :id
    Belge Indir :id
*/

router.get('/',                FaturaController.faturalarView)

module.exports = router
