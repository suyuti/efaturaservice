const mongoose = require('mongoose')
const MONGO_URI = process.env.MONGO_URI;

module.exports = () => {
    const connect = () => {
        mongoose.connect(MONGO_URI, {
            keepAlive: true,
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useFindAndModify: false
        }).then(() => {
            console.log('DB OK')
        }, err => {
            console.log(`DB error `, err)
        })
    }
    connect()
}
