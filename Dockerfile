FROM node:12.18.3
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install
COPY . .
ENV MONGO_URI=mongodb://experto:suyuti9419@161.35.143.57:27017/ExpertoDb
ENV AMQP_URL=amqp://157.245.143.143:5672
ENV FATURA_QUEUE=experto-faturagonder-test
ENV PORT=5000
EXPOSE 5000
CMD ["node", "index.js"]
