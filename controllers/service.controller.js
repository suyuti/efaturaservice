var winston = require('../config/winston.config');
var AmqpService = require('../services/amqp2.service')

var t = false
exports.service = async (req, res) => {
    let listening = AmqpService.isListening()
    res.render('service', { listening: listening })
}

exports.serviceToggle = async (req, res) => {
    if (AmqpService.isListening()) {
        AmqpService.stop().then(resp => {
            res.send({ listening: resp.connect, queueName: '' })
        })
    }
    else {
        AmqpService.start()
        //.then(resp => {
        //    res.send({ listening: resp.connect, queueName: resp.queue })
        //})
        //.catch((reason) => {
        //    res.send({ durum: false })
        //})
    }
}

exports.gonderx10 = async (req, res) => {
    if (AmqpService.isListening()) {
        AmqpService.stop().then(resp => {
            res.send({ listening: resp.connect, queueName: '' })
        })
    }
    else {
        AmqpService.start().then(resp => {
            res.send({ listening: resp.connect, queueName: resp.queue })
        })
        .catch((reason) => {
            res.send({ durum: false })
        })
    }
}
