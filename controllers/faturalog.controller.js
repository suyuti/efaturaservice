var winston = require('../config/winston.config');
const EFaturaService = require('../services/efatura.service')
const FirmaModel = require('../models/firma.model')
const TenantModel = require('../models/tenant.model')
const FaturaLogModel = require('../models/faturalog.model')
const moment = require('moment')

exports.getAll = async (req, res) => {
    winston.log('error', 'faturalar')
    let logs = await FaturaLogModel.find().limit(200).populate('fatura').sort({date: -1}).lean(true).exec()
    res.render('faturalog', {logs:logs, moment})
}
