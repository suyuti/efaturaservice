var winston = require('../config/winston.config');
const EFaturaService = require('../services/efatura.service')
const FirmaModel = require('../models/firma.model')
const TenantModel = require('../models/tenant.model')
const FaturaModel = require('../models/fatura.model')
const moment = require('moment')

exports.faturalar = async (req, res) => {
    winston.log('error', 'faturalar')
    return res.status(200).json({error: null, data: null})
}

exports.faturalarView = async (req, res) => {
    let faturalar = await FaturaModel.find().sort({duzenlemeTarihi: -1}).populate('firma').populate('kesenFirma').lean(true).exec()
    res.render('faturalar', {faturalar: faturalar, moment})
}
