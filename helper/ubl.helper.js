const numbro = require('numbro')
const moment = require('moment')

module.exports = makeUbl = (fatura, kalemler) => {
    var accountingSupplierParty = `
    <cac:AccountingSupplierParty>
        <cac:Party>
            <cac:PartyIdentification>
                <cbc:ID schemeID="VKN">${fatura.kesenFirma.vkn}</cbc:ID>
            </cac:PartyIdentification>
            <cac:PartyName>
                <cbc:Name>${fatura.kesenFirma.unvan}</cbc:Name>
            </cac:PartyName>
            <cac:PostalAddress>
                <cbc:StreetName>${fatura.kesenFirma.adres}</cbc:StreetName>
                <cbc:CitySubdivisionName>${fatura.kesenFirma.ilce}</cbc:CitySubdivisionName>
                <cbc:CityName>${fatura.kesenFirma.il}</cbc:CityName>
                <cac:Country>
                    <cbc:Name>${fatura.kesenFirma.ulke}</cbc:Name>
                </cac:Country>
            </cac:PostalAddress>
            <cac:PartyTaxScheme>
                <cac:TaxScheme>
                    <cbc:Name>${fatura.kesenFirma.vd}</cbc:Name>
                </cac:TaxScheme>
            </cac:PartyTaxScheme>
            <cac:Contact>
                <cbc:Telephone>${fatura.kesenFirma.kontakTelefon}</cbc:Telephone>
                <cbc:ElectronicMail>${fatura.kesenFirma.kontakMail}</cbc:ElectronicMail>
            </cac:Contact>
        </cac:Party>
    </cac:AccountingSupplierParty>`;

    var accountingCustomerParty = ''
    
    if (fatura.firma.efaturaTCKNKullan) {
        accountingCustomerParty = `
        <cac:AccountingCustomerParty>
            <cac:Party>
                <cac:PartyIdentification>
                    <cbc:ID schemeID="TCKN">${fatura.firma.efaturaTCKN}</cbc:ID>
                </cac:PartyIdentification>
                <cac:PostalAddress>
                    <cbc:StreetName>${fatura.firma.adres}</cbc:StreetName>
                    <cbc:CitySubdivisionName>${fatura.firma.ilce}</cbc:CitySubdivisionName>
                    <cbc:CityName>${fatura.firma.il}</cbc:CityName>
                    <cac:Country>
                        <cbc:Name>${fatura.firma.ulke}</cbc:Name>
                    </cac:Country>
                </cac:PostalAddress>
                <cac:Person>
                    <cbc:FirstName>${fatura.firma.efaturaTCKNAdi}</cbc:FirstName>
                    <cbc:FamilyName>${fatura.firma.efaturaTCKNSoyadi}</cbc:FamilyName>
                </cac:Person>
            </cac:Party>
        </cac:AccountingCustomerParty>`
    }
    else {
        accountingCustomerParty = `
            <cac:AccountingCustomerParty>
                <cac:Party>
                    <cac:PartyIdentification>
                        <cbc:ID schemeID="VKN">${fatura.firma.vergiNo}</cbc:ID>
                    </cac:PartyIdentification>
                    <cac:PartyName>
                        <cbc:Name>${fatura.firma.unvan}</cbc:Name>
                    </cac:PartyName>
                    <cac:PostalAddress>
                        <cbc:StreetName>${fatura.firma.adres}</cbc:StreetName>
                        <cbc:CitySubdivisionName>${fatura.firma.ilce}</cbc:CitySubdivisionName>
                        <cbc:CityName>${fatura.firma.il}</cbc:CityName>
                        <cac:Country>
                            <cbc:Name>${fatura.firma.ulke}</cbc:Name>
                        </cac:Country>
                    </cac:PostalAddress>
                    <cac:PartyTaxScheme>
                        <cac:TaxScheme>
                            <cbc:Name>${fatura.firma.vergiDairesi}</cbc:Name>
                        </cac:TaxScheme>
                    </cac:PartyTaxScheme>
                </cac:Party>
            </cac:AccountingCustomerParty>`;
    }

    var signature = `
    <cac:Signature>
        <cbc:ID schemeID="VKN_TCKN">${fatura.kesenFirma.vkn}</cbc:ID>
        <cac:SignatoryParty>
            <cac:PartyIdentification>
                <cbc:ID schemeID="VKN">${fatura.kesenFirma.vkn}</cbc:ID>
            </cac:PartyIdentification>
            <cac:PostalAddress>
                <cbc:StreetName>${fatura.kesenFirma.adres}</cbc:StreetName>
                <cbc:CitySubdivisionName>${fatura.kesenFirma.ilce}</cbc:CitySubdivisionName>
                <cbc:CityName>${fatura.kesenFirma.il}</cbc:CityName>
                <cac:Country>
                    <cbc:Name>${fatura.kesenFirma.ulke}</cbc:Name>
                </cac:Country>
            </cac:PostalAddress>
        </cac:SignatoryParty>
        <cac:DigitalSignatureAttachment>
            <cac:ExternalReference>
                <cbc:URI>#Signature</cbc:URI>
            </cac:ExternalReference>
        </cac:DigitalSignatureAttachment>
    </cac:Signature>`;


    var lines = ''
    var i = 1
    var kdvler = [
        { oran: 0, fiyat: 0, kdv: 0, adet: 0 },
        { oran: 1, fiyat: 0, kdv: 0, adet: 0 },
        { oran: 8, fiyat: 0, kdv: 0, adet: 0 },
        { oran: 18, fiyat: 0, kdv: 0, adet: 0 },
    ]

    let faturaFiyat = 0
    let faturaToplam = 0
    let faturaKdv = 0

    for (var k of kalemler) {
        let adet = parseInt(k.miktar)
        let birimFiyat = parseFloat(k.birimFiyat)
        let kdvOran = parseInt(k.kdvOran)
        //let toplam      = parseFloat(k.toplam)

        let fiyat = adet * birimFiyat
        let kdv = kdvOran * fiyat / 100
        let toplam = fiyat + kdv
        let tumKdvler = kdvler.find(_kdv => _kdv.oran == kdvOran)
        tumKdvler.kdv += kdv
        tumKdvler.fiyat += fiyat
        tumKdvler.adet++

        faturaFiyat += fiyat
        faturaToplam += toplam
        faturaKdv += kdv

        lines = lines + `
        <cac:InvoiceLine>
            <cbc:ID>${i}</cbc:ID>
            <cbc:InvoicedQuantity unitCode="C62">${adet}</cbc:InvoicedQuantity>
            <cbc:LineExtensionAmount currencyID="TRY">${numbro(fiyat).format({ mantissa: 2 })}</cbc:LineExtensionAmount>
            <cac:AllowanceCharge>
                <cbc:ChargeIndicator>true</cbc:ChargeIndicator>
                <cbc:AllowanceChargeReason/>
                <cbc:MultiplierFactorNumeric>0</cbc:MultiplierFactorNumeric>
                <cbc:SequenceNumeric>0</cbc:SequenceNumeric>
                <cbc:Amount currencyID="TRY">0</cbc:Amount>
                <cbc:BaseAmount currencyID="TRY">${numbro(birimFiyat).format({ mantissa: 2 })}</cbc:BaseAmount>
            </cac:AllowanceCharge>
            <cac:TaxTotal>
                <cbc:TaxAmount currencyID="TRY">${numbro(kdv).format({ mantissa: 2 })}</cbc:TaxAmount>
                <cac:TaxSubtotal>
                    <cbc:TaxableAmount currencyID="TRY">${numbro(fiyat).format({ mantissa: 2 })}</cbc:TaxableAmount>
                    <cbc:TaxAmount currencyID="TRY">${numbro(kdv).format({ mantissa: 2 })}</cbc:TaxAmount>
                    <cbc:Percent>${kdvOran}</cbc:Percent>
                    <cac:TaxCategory>
                        <cac:TaxScheme>
                            <cbc:Name>KDV</cbc:Name>
                            <cbc:TaxTypeCode>0015</cbc:TaxTypeCode>
                        </cac:TaxScheme>
                    </cac:TaxCategory>
                </cac:TaxSubtotal>
            </cac:TaxTotal>
            <cac:Item>
                <cbc:Name>
                    <![CDATA[ ${k.urun ? k.urun.adi : ''} ${k.aciklama} ]]>
                </cbc:Name>
            </cac:Item>
            <cac:Price>
                <cbc:PriceAmount currencyID="TRY">${numbro(birimFiyat).format({ mantissa: 2 })}</cbc:PriceAmount>
            </cac:Price>
        </cac:InvoiceLine>\n`
        i++
    }

    var taxTotal = `
    <cac:TaxTotal>
        <cbc:TaxAmount currencyID="TRY">${numbro(faturaKdv).format({ mantissa: 2 })}</cbc:TaxAmount>
        `
    for (let kdv of kdvler) {
        if (kdv.kdv == 0) continue
        taxTotal += `
        <cac:TaxSubtotal>
        <cbc:TaxableAmount currencyID="TRY">${numbro(kdv.fiyat).format({ mantissa: 2 })}</cbc:TaxableAmount>
        <cbc:TaxAmount currencyID="TRY">${numbro(kdv.kdv).format({ mantissa: 2 })}</cbc:TaxAmount>
        <cbc:Percent>${kdv.oran}</cbc:Percent>
        <cac:TaxCategory>
            <cac:TaxScheme>
                <cbc:TaxTypeCode>0015</cbc:TaxTypeCode>
            </cac:TaxScheme>
        </cac:TaxCategory>
        </cac:TaxSubtotal>
        `
    }
    taxTotal += `
    </cac:TaxTotal>`;


    var legalMonetaryTotal = `
    <cac:LegalMonetaryTotal>
        <cbc:LineExtensionAmount currencyID="TRY">${numbro(faturaFiyat).format({ mantissa: 2 })}</cbc:LineExtensionAmount>
        <cbc:TaxExclusiveAmount currencyID="TRY">${numbro(faturaFiyat).format({ mantissa: 2 })}</cbc:TaxExclusiveAmount>
        <cbc:TaxInclusiveAmount currencyID="TRY">${numbro(faturaToplam).format({ mantissa: 2 })}</cbc:TaxInclusiveAmount>
        <cbc:AllowanceTotalAmount currencyID="TRY">0.00</cbc:AllowanceTotalAmount>
        <cbc:ChargeTotalAmount currencyID="TRY">0.00</cbc:ChargeTotalAmount>
        <cbc:PayableRoundingAmount currencyID="TRY">0</cbc:PayableRoundingAmount>
        <cbc:PayableAmount currencyID="TRY">${numbro(faturaToplam).format({ mantissa: 2 })}</cbc:PayableAmount>
    </cac:LegalMonetaryTotal>`;

    let iade = ``
    if (fatura.faturaTuru == 'IADE') {
        iade = `<cac:BillingReference>
        <cac:InvoiceDocumentReference>
          <cbc:ID>${fatura.iadeFaturaNo}</cbc:ID>
          <cbc:IssueDate>${fatura.iadeFaturaTarih}</cbc:IssueDate>
          <cbc:DocumentType>FATURA</cbc:DocumentType>
        </cac:InvoiceDocumentReference>
      </cac:BillingReference>`
    }



    var _ubl = [`<?xml version="1.0" encoding="UTF-8"?>
            <?xml-stylesheet type="text/xsl" href="general.xslt"?>
            <Invoice 
                xsi:schemaLocation="urn:oasis:names:specification:ubl:schema:xsd:Invoice-2 UBL-Invoice-2.1.xsd" 
                xmlns="urn:oasis:names:specification:ubl:schema:xsd:Invoice-2" 
                xmlns:n4="http://www.altova.com/samplexml/other-namespace" 
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
                xmlns:cac="urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2" 
                xmlns:cbc="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2" 
                xmlns:ext="urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2"
                xmlns:ccts="urn:un:unece:uncefact:documentation:2" xmlns:ds="http://www.w3.org/2000/09/xmldsig#" 
                xmlns:qdt="urn:oasis:names:specification:ubl:schema:xsd:QualifiedDatatypes-2" 
                xmlns:ubltr="urn:oasis:names:specification:ubl:schema:xsd:TurkishCustomizationExtensionComponents" 
                xmlns:udt="urn:un:unece:uncefact:data:specification:UnqualifiedDataTypesSchemaModule:2" 
                xmlns:xades="http://uri.etsi.org/01903/v1.3.2#">
                <ext:UBLExtensions>
                    <ext:UBLExtension>
                        <ext:ExtensionContent>
                            <n4:auto-generated_for_wildcard/>
                        </ext:ExtensionContent>
                    </ext:UBLExtension>
                </ext:UBLExtensions>
                <cbc:UBLVersionID>2.1</cbc:UBLVersionID>
                <cbc:CustomizationID>TR1.2</cbc:CustomizationID>
                <cbc:ProfileID>TEMELFATURA</cbc:ProfileID>
                <cbc:ID>${fatura.faturaNo}</cbc:ID>
                <cbc:CopyIndicator>false</cbc:CopyIndicator>
                <cbc:UUID>${fatura.key}</cbc:UUID>
                <cbc:IssueDate>${moment(fatura.duzenlemeTarihi).format('YYYY-MM-DD')}</cbc:IssueDate>
                <cbc:InvoiceTypeCode>${fatura.faturaTuru == 'SATIS' ? 'SATIS' : 'IADE'}</cbc:InvoiceTypeCode>
                <cbc:Note>${fatura.aciklama}</cbc:Note>
                <cbc:DocumentCurrencyCode>TRY</cbc:DocumentCurrencyCode>
                <cbc:AccountingCost/>
                <cbc:LineCountNumeric>${kalemler.length}</cbc:LineCountNumeric>`,
        iade,
        signature,
        accountingSupplierParty,
        accountingCustomerParty,
        taxTotal,
        legalMonetaryTotal,
        lines,
        `</Invoice>`,
    ];

    return _ubl.join('\n')
}