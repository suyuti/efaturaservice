process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;
const express = require("express");
const app = express();
//const http = require('http').Server(app);
const server = require('http').createServer(app);
const router = express.Router()
const initMongo = require('./config/mongo.config')
var path = require("path");
var port = process.env.PORT || 8080;

app.set("views", __dirname + "/views");
app.set('view engine', 'pug')
app.use(function (req, res, next) {
    app.locals.pretty = true;
    next();
});

app.use(express.static(path.join(__dirname, "public")));

app.use('/', require('./router'))

initMongo()

const io = require('socket.io')(server);
global.io = io
io.on('connection', client => {
    console.log('Connected')
  client.on('event', data => { /* … */ });
  client.on('disconnect', () => { /* … */ });
});

server.listen(port, () => {
  console.log(`Listening on ${port}`)
});

//http.listen(port, () => {
//    console.log(`Listening on ${port}`)
//})
